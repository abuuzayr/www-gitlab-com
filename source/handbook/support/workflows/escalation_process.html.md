---
layout: markdown_page
title: Support <> TAM Escalations
category: Handling tickets
---

### When to escalate a ticket to a Technical Account Manager (TAM)

If a customer submits either an Emergency or High Priority ticket and there is a TAM associated with the account, copy the TAM onto the ticket and @ mention them on Slack with the ticket details.  

Details on the TAM Escalation process can be found here:  
[Customer Success Escalation Process](/handbook/customer-success/tam/escalations/index.html)
