| Priority | Description | Issue label(s) |
| ------ | ------ | ------ |
| 1 | <a href="/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security fixes</a> | `security` |
| 2 | Data-loss prevention | `data loss` | 
| 3 | Availability | `availability` | 
| 4 | Fixing regressions (things that worked before) | `regression` |
| 5 | Velocity of new features, user experience improvements, technical debt, community contributions, and all other improvements | `feature`, `enhancement`, `technical debt` |
| 6 | Behaviors that yield higher predictability (because this inevitably slows us down) | `predictability` |